#
#
# route.init
#
#

##
#Save ip Vars for each device.
##
#eth0, Local network.
ETH0IP=$(returnipraw eth0)
#ETH0BCAST=$(returnipbcastraw eth0)
#ETH0MASK=$(returnipmaskraw eth0)
ETH0NETWORK=$(returnipnetworkraw eth0)
ETH0CIDR=$(returnipcidrraw eth0)
ETH0GATEWAY=$(returnipgatewayraw eth0)

#eth1, ISP1
ETH1IP=$(returnipraw eth1)
#ETH1BCAST=$(returnipbcastraw eth1)
#ETH1MASK=$(returnipmaskraw eth1)
ETH1NETWORK=$(returnipnetworkraw eth1)
ETH1CIDR=$(returnipcidrraw eth1)
ETH1GATEWAY=$(returnipgatewayraw eth1)

#eth2, ISP2
ETH2IP=$(returnipraw eth2)
#ETH2BCAST=$(returnipbcastraw eth2)
#ETH2MASK=$(returnipmaskraw eth2)
ETH2NETWORK=$(returnipnetworkraw eth2)
ETH2CIDR=$(returnipcidrraw eth2)
ETH2GATEWAY=$(returnipgatewayraw eth2)

#eth3, ISP3
ETH3IP=$(returnipraw eth3)
#ETH3BCAST=$(returnipbcastraw eth3)
#ETH3MASK=$(returnipmaskraw eth3)
ETH3NETWORK=$(returnipnetworkraw eth3)
ETH3CIDR=$(returnipcidrraw eth3)
ETH3GATEWAY=$(returnipgatewayraw eth3)

#eth4, ISP4
ETH4IP=$(returnipraw eth4)
#ETH4BCAST=$(returnipbcastraw eth4)
#ETH4MASK=$(returnipmaskraw eth4)
ETH4NETWORK=$(returnipnetworkraw eth4)
ETH4CIDR=$(returnipcidrraw eth4)
ETH4GATEWAY=$(returnipgatewayraw eth4)



##
#routing setup.
##

#tables declared in /etc/iproute2/rt_tables
#1 cb1
#2 cb2
#3 cb3
#4 cb4

#flush everything, iplement when new method to get gateway.
##no, never flush, bad things happen.
#ip route flush all
#ip route flush all table cb1
#ip route flush all table cb2
#ip route flush all table cb3
#ip route flush all table cb4

#replace main routes
#already defined in route-eth0
#route add -net 192.168.2.0 netmask 255.255.255.0 eth0

#setup routes in tables
ip route replace $ETH1NETWORK$ETH1CIDR dev eth1 proto kernel scope link src $ETH1IP table cb1
ip route replace default via $ETH1GATEWAY dev eth1 table cb1
ip route replace $ETH2NETWORK$ETH2CIDR dev eth2 proto kernel scope link src $ETH2IP table cb2
ip route replace default via $ETH2GATEWAY dev eth2 table cb2
ip route replace $ETH3NETWORK$ETH3CIDR dev eth3 proto kernel scope link src $ETH3IP table cb3
ip route replace default via $ETH3GATEWAY dev eth3 table cb3
ip route replace $ETH4NETWORK$ETH4CIDR dev eth4 proto kernel scope link src $ETH4IP table cb4
ip route replace default via $ETH4GATEWAY dev eth4 table cb4

#set pub to private routes on each table
ip route replace $ETH0NETWORK$ETH0CIDR dev eth0 proto kernel scope link src $ETH0IP table cb1
ip route replace $ETH2NETWORK$ETH2CIDR dev eth2 proto kernel scope link src $ETH2IP table cb1
ip route replace $ETH3NETWORK$ETH3CIDR dev eth3 proto kernel scope link src $ETH3IP table cb1
ip route replace $ETH4NETWORK$ETH4CIDR dev eth4 proto kernel scope link src $ETH4IP table cb1
ip route replace 127.0.0.0/8 	   dev lo   table cb1

ip route replace $ETH0NETWORK$ETH0CIDR dev eth0 proto kernel scope link src $ETH0IP table cb2
ip route replace $ETH1NETWORK$ETH1CIDR dev eth1 proto kernel scope link src $ETH1IP table cb2
ip route replace $ETH3NETWORK$ETH3CIDR dev eth3 proto kernel scope link src $ETH3IP table cb2
ip route replace $ETH4NETWORK$ETH4CIDR dev eth4 proto kernel scope link src $ETH4IP table cb2
ip route replace 127.0.0.0/8	   dev lo   table cb2

ip route replace $ETH0NETWORK$ETH0CIDR dev eth0 proto kernel scope link src $ETH0IP table cb3
ip route replace $ETH1NETWORK$ETH1CIDR dev eth1 proto kernel scope link src $ETH1IP table cb3
ip route replace $ETH2NETWORK$ETH2CIDR dev eth2 proto kernel scope link src $ETH2IP table cb3
ip route replace $ETH4NETWORK$ETH4CIDR dev eth4 proto kernel scope link src $ETH4IP table cb3
ip route replace 127.0.0.0/8	   dev lo   table cb3

ip route replace $ETH0NETWORK$ETH0CIDR dev eth0 proto kernel scope link src $ETH0IP table cb4
ip route replace $ETH1NETWORK$ETH1CIDR dev eth1 proto kernel scope link src $ETH1IP table cb4
ip route replace $ETH2NETWORK$ETH2CIDR dev eth2 proto kernel scope link src $ETH2IP table cb4
ip route replace $ETH3NETWORK$ETH3CIDR dev eth3 proto kernel scope link src $ETH3IP table cb4
ip route replace 127.0.0.0/8	   dev lo   table cb4


#listen for upnp requests for ushare
ip route replace 239.0.0.0/8 dev eth0

#set default route
ip route replace default via $ETH1GATEWAY dev eth1

#debug to test vars
#echo $ETH0IP $ETH0NETWORK$ETH0CIDR;
#echo $ETH1IP $ETH1NETWORK$ETH1CIDR;
#echo $ETH2IP $ETH2NETWORK$ETH2CIDR;
#echo $ETH3IP $ETH3NETWORK$ETH3CIDR;
#echo $ETH4IP $ETH2NETWORK$ETH4CIDR;
