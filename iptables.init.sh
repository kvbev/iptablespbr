#
#
#sets default firewall/routing state. clears all other rules.
#
#

#set ${IPTABLES} var
IPTABLES=/usr/sbin/iptables

#flush everything
${IPTABLES} -F
${IPTABLES} -F -t nat
${IPTABLES} -F -t mangle
${IPTABLES} -X -t nat
${IPTABLES} -X -t mangle
${IPTABLES} -X

#set default policies
${IPTABLES} -P INPUT DROP
${IPTABLES} -P OUTPUT DROP
${IPTABLES} -P FORWARD DROP

#accept all loopback connections
${IPTABLES} -A INPUT  -i lo -j ACCEPT 
${IPTABLES} -A OUTPUT  -o lo -j ACCEPT

#setup masquerading
${IPTABLES} -t nat -A POSTROUTING -s 192.168.2.0/255.255.255.0 -o eth1 -j MASQUERADE
${IPTABLES} -t nat -A POSTROUTING -s 192.168.2.0/255.255.255.0 -o eth2 -j MASQUERADE
${IPTABLES} -t nat -A POSTROUTING -s 192.168.2.0/255.255.255.0 -o eth3 -j MASQUERADE
${IPTABLES} -t nat -A POSTROUTING -s 192.168.2.0/255.255.255.0 -o eth4 -j MASQUERADE


#INPUT rules

#setup input rules global
${IPTABLES} -A INPUT -p icmp -m state --state RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -p tcp -m tcp --sport 53 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT

#setup input rules eth0
${IPTABLES} -A INPUT -s 192.168.2.0/255.255.255.0 -i eth0 -j ACCEPT 

#setup input rules eth1
${IPTABLES} -A INPUT -i eth1 -p icmp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -i eth1 -p tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -i eth1 -p udp -m state --state RELATED,ESTABLISHED -j ACCEPT

#setup input rules eth2
${IPTABLES} -A INPUT -i eth2 -p icmp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -i eth2 -p tcp -m tcp --dport 22 -j ACCEPT
${IPTABLES} -A INPUT -i eth2 -p tcp -m tcp --dport 54321 -j ACCEPT
${IPTABLES} -A INPUT -i eth2 -p tcp -m tcp --dport 54322 -j ACCEPT
${IPTABLES} -A INPUT -i eth2 -p tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -i eth2 -p udp -m state --state RELATED,ESTABLISHED -j ACCEPT

#setup input rules eth3
${IPTABLES} -A INPUT -i eth3 -p tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -i eth3 -p udp -m state --state RELATED,ESTABLISHED -j ACCEPT

#setup input rules eth4
${IPTABLES} -A INPUT -i eth4 -p tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A INPUT -i eth4 -p udp -m state --state RELATED,ESTABLISHED -j ACCEPT


#OUTPUT rules

#setup output rules global
${IPTABLES} -A OUTPUT -p icmp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A OUTPUT -p tcp -m tcp --sport 1024:65535 --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT

#setup output rules eth0
${IPTABLES} -A OUTPUT -d 192.168.2.0/255.255.255.0 -o eth0 -j ACCEPT

#setup output rules eth1
${IPTABLES} -A OUTPUT -o eth1 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A OUTPUT -o eth1 -p udp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

#setup output rules eth2
${IPTABLES} -A OUTPUT -o eth2 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A OUTPUT -o eth2 -p udp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

#setup output rules eth3
${IPTABLES} -A OUTPUT -o eth3 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A OUTPUT -o eth3 -p udp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

#setup output rules eth4
${IPTABLES} -A OUTPUT -o eth4 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
${IPTABLES} -A OUTPUT -o eth4 -p udp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT


#FORWARD rules

#setup forward rules global
#${IPTABLES} -A FORWARD -p tcp -m tcp --sport 1024:65535 --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
#${IPTABLES} -A FORWARD -p tcp -m tcp --sport 53 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT


#setup forward rules eth0
${IPTABLES} -A FORWARD -s 192.168.2.0/255.255.255.0 -i eth0 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

#setup forward rules eth1
${IPTABLES} -A FORWARD -i eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT

#setup forward rules eth2
${IPTABLES} -A FORWARD -i eth2 -p tcp -m state --state NEW --dport 51001:51002 -j ACCEPT
${IPTABLES} -A FORWARD -i eth2 -m state --state RELATED,ESTABLISHED -j ACCEPT

#setup forward rules eth3
${IPTABLES} -A FORWARD -i eth3 -p tcp -m state --state NEW --dport 53001:53002 -j ACCEPT
${IPTABLES} -A FORWARD -i eth3 -m state --state RELATED,ESTABLISHED -j ACCEPT

#setup forward rules eth4
${IPTABLES} -A FORWARD -i eth4 -p tcp -m state --state NEW --dport 54001:54002 -j ACCEPT
${IPTABLES} -A FORWARD -i eth4 -m state --state RELATED,ESTABLISHED -j ACCEPT


# PREROUTING Rules, port forwarding

#port forward ports eth2
${IPTABLES} -t nat -A PREROUTING -i eth2 -p tcp --dport 51001 -j DNAT --to-destination 192.168.2.11
${IPTABLES} -t nat -A PREROUTING -i eth2 -p tcp --dport 51002 -j DNAT --to-destination 192.168.2.12

#port forward ports eth3
${IPTABLES} -t nat -A PREROUTING -i eth3 -p tcp --dport 53001 -j DNAT --to-destination 192.168.2.31
${IPTABLES} -t nat -A PREROUTING -i eth3 -p tcp --dport 53002 -j DNAT --to-destination 192.168.2.32

#port forward ports eth4
${IPTABLES} -t nat -A PREROUTING -i eth4 -p tcp --dport 54001 -j DNAT --to-destination 192.168.2.41
${IPTABLES} -t nat -A PREROUTING -i eth4 -p tcp --dport 54002 -j DNAT --to-destination 192.168.2.42


#mark traffic.

#mangle traffic to specific tables. cb1 mark 1, cb2 mark 2, cb3 mark 3, cb4 mark 4
${IPTABLES} -t mangle -A PREROUTING -i eth0 -p tcp --dport 80 -s 192.168.2.0/255.255.255.0 -d 192.168.100.11 -j MARK --set-mark 1
${IPTABLES} -t mangle -A PREROUTING -i eth0 -p tcp --dport 80 -s 192.168.2.0/255.255.255.0 -d 192.168.100.12 -j MARK --set-mark 2
${IPTABLES} -t mangle -A PREROUTING -i eth0 -p tcp --dport 80 -s 192.168.2.0/255.255.255.0 -d 192.168.100.13 -j MARK --set-mark 3
${IPTABLES} -t mangle -A PREROUTING -i eth0 -p tcp --dport 80 -s 192.168.2.0/255.255.255.0 -d 192.168.100.14 -j MARK --set-mark 4

#${IPTABLES} -t mangle -A PREROUTING -i eth0 -p udp -j MARK --set-mark 4

${IPTABLES} -t mangle -A PREROUTING -i eth0 -p tcp -m multiport --dports 110,119,563 -s 192.168.2.0/255.255.255.0 -j MARK --set-mark 2
${IPTABLES} -t mangle -A PREROUTING -i eth0 -p udp -m multiport --dports 110,119,563 -s 192.168.2.0/255.255.255.0 -j MARK --set-mark 2

#${IPTABLES} -t mangle -A PREROUTING -i eth0 -p tcp --sport 80 -s 192.168.2.0/255.255.255.0 -j MARK --set-mark 3
#${IPTABLES} -t mangle -A PREROUTING -i eth0 -p tcp --sport 443 -s 192.168.2.0/255.255.255.0 -j MARK --set-mark 3

#mark all traffic from specific ip's eth2 to cb2
#${IPTABLES} -t mangle -A PREROUTING -i eth0 -s 192.168.2.42 -j MARK --set-mark 2
#${IPTABLES} -t mangle -A PREROUTING -i eth0 -s 192.168.2.43 -j MARK --set-mark 2



