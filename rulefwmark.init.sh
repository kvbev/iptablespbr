#
#
# rulefwmark.init.sh
#
# set fwmarks to routing tables

#delete existing ip entrys
ip rule del priority 301 table cb1
ip rule del priority 302 table cb2
ip rule del priority 303 table cb3
ip rule del priority 304 table cb4

#set marks to route traffic from iptables
ip rule add priority 301 fwmark 1 table cb1
ip rule add priority 302 fwmark 2 table cb2
ip rule add priority 303 fwmark 3 table cb3
ip rule add priority 304 fwmark 4 table cb4

#output rule list to verify no duplicates
ip rule show