#
#
# ruleip.init.sh shenanigans
#
#

#get ips
eth1ip=$(returnipraw eth1)
eth2ip=$(returnipraw eth2)
eth3ip=$(returnipraw eth3)
eth4ip=$(returnipraw eth4)

#delete existing ip entrys
ip rule del priority 201 table cb1
ip rule del priority 202 table cb2
ip rule del priority 203 table cb3
ip rule del priority 204 table cb4

#set ips to their tables
ip rule add priority 201 from $eth1ip table cb1
ip rule add priority 202 from $eth2ip table cb2
ip rule add priority 203 from $eth3ip table cb3
ip rule add priority 204 from $eth4ip table cb4
